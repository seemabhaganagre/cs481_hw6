﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hw6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();




            async void When_Clicked(object sender, System.EventArgs e)
            {


                HttpClient client = new HttpClient();
                string st = search.Text;
                var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + st));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                HttpResponseMessage response = await client.SendAsync(request);
                DictionaryData[] d = null;
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    d = DictionaryData.FromJson(content);
                    wordType.Text = $"Type is {d[0].Type}";
                    wordDef.Text = $"Word is {d[1].Def}";

                    anExample.Text = $"Example: {d[2].Example}";
                }
            }

        }
    }






